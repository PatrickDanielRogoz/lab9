package rogoz.patrick.lab9.Ex2;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Count extends JFrame {

    private int counter = 0;

    public Count() {
        super("Incrementer");
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        JPanel layout = new JPanel();
        JLabel counterLbl = new JLabel(Integer.toString(counter));
        layout.add(counterLbl);

        JButton incButton = new JButton("Click to Increment");
        incButton.addActionListener(l -> {
            counterLbl.setText(Integer.toString(++counter));
        });
        layout.add(incButton);

        add(layout);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Count());
    }
}