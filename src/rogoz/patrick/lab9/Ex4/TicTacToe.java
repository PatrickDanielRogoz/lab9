package rogoz.patrick.lab9.Ex4;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



public class TicTacToe implements ActionListener {



    JFrame window = new JFrame("Tic-Tac-Toe");
    JButton[] button;
    String letter = "";
    public int count = 0;
    public boolean win = false;

    public TicTacToe(){

        button = new JButton[9];

        window.setSize(300,300);
        window.setLayout(new GridLayout(3,3));


        JButton dummy = new JButton("");
        Font font = dummy.getFont();
        Font bigFont = font.deriveFont(font.getSize2D() * 5.0f);

        // create the buttons
        for(int i = 0; i < 9; i++) {
            button[i] = new JButton("");
            button[i].setFont(bigFont);
            button[i].addActionListener(this);
            window.add(button[i]);
        }

        window.setVisible(true);
    }public void actionPerformed(ActionEvent a) {
        count++;
        if(count % 2 == 1)
            letter = "X";
        else
            letter = "O";
        Object but = a.getSource();
        for(int i = 0; i < 9; i++) {
            if(but == button[i]) {
                button[i].setText(letter);
                button[i].setEnabled(false);
                break;
            }
        }
//		horizontal wins
        if( button[0].getText() == button[1].getText() && button[1].getText() == button[2].getText() && button[0].getText() != ""){
            win = true;
        }
        else if(button[3].getText() == button[4].getText() && button[4].getText() == button[5].getText() && button[3].getText() != ""){
            win = true;
        }
        else if(button[6].getText() == button[7].getText() && button[7].getText() == button[8].getText() && button[6].getText() != ""){
            win = true;
        }//vertical wins
        else if(button[0].getText() == button[3].getText() && button[3].getText() == button[6].getText() && button[0].getText() != ""){
            win = true;
        }
        else if(button[1].getText() == button[4].getText() && button[4].getText() == button[7].getText() && button[1].getText() != ""){
            win = true;
        }
        else if(button[2].getText() == button[5].getText() && button[5].getText() == button[8].getText() && button[2].getText() != ""){
            win = true;
        }//diagonal wins
        else if(button[0].getText() == button[4].getText() && button[4].getText() == button[8].getText() && button[0].getText() != ""){
            win = true;
        }
        else if(button[2].getText() == button[4].getText() && button[4].getText() == button[6].getText() && button[2].getText() != ""){
            win = true;
        }
        else {
            win = false;
        }
        if(win == true){
            JOptionPane.showMessageDialog(null, letter + " WINS!");
        } else if(count == 9 && win == false){
            JOptionPane.showMessageDialog(null, "Tie Game!");
        }
    }public static void main(String[] args){

        new TicTacToe();
    }
}

